
let Employee = require('../models/employersM');
const mongoose = require('mongoose');


exports.employeeList = function (req, res) {
  Employee.find((err, docs) => {
    if(!err) {
      res.render('list', {
        list: docs
      })
    }
    else{
      console.log('Error in retrieving employee list: ' + err)
    }
  })
}

exports.employeeCreatePage = function(req, res) {
    res.render('index', { title: 'Add Employee', employee: {} });
}

exports.employeeCreateEmployee = function (req, res) {
  if(req.body._id == ''){
    insertRecord(req, res);
  } else{
    updateRecord(req,res)
  }
}

function handleValidationError(err, body) {
  for (field in err.errors) {
      switch (err.errors[field].path) {
          case 'fullName':
              body['fullNameError'] = err.errors[field].message;
              break;
          case 'email':
              body['emailError'] = err.errors[field].message;
              break;
          default:
              break;
      }
  }
}

exports.employeeUpdatePage = function (req, res) {
  Employee.findById(req.params.id, (err, doc) => {
    if(!err) {
      res.render('index', {
        title: 'Update Employee',
        employee: doc
      })
    }
  })
}


function updateRecord (req, res) {
  Employee.findByIdAndUpdate({_id: req.body._id}, req.body, {new: true}, (err, doc) => {
    if (!err) {res.redirect('/employers')}
    else {
      if (err.name == 'ValidationError') {
        handleValidationError(err, req.body);
        res.render('index', {
          title: 'Update Employee',
          employee: req.body
        })
      } else {
        console.log('Error during record update: ' + err)
      }
    }
  });
}

function insertRecord (req, res) {
  let employee = new Employee();
  employee.fullName = req.body.fullName;
  employee.email = req.body.email;
  employee.mobile = req.body.mobile;
  employee.city = req.body.city;

  employee.save((err, doc) => {
    if(!err){
      res.redirect('/employers')
    }
    else{
      if(err.name == 'ValidationError') {
        handleValidationError(err, req.body);
        res.render('index', {
          title: "Add Employee",
          employee: req.body
        })
      }else{
        console.log('Error during insertion: ' + err)
      }
     
    }
  })
}

exports.employeeDelete = function (req, res){
  Employee.findByIdAndDelete(req.params.id, (err, doc) => {
    if(!err){
      res.redirect('/employers')
    } else{
      console.log('Error in employee delete: ' + err)
    }
  })
}