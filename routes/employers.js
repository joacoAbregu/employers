var express = require('express');
var router = express.Router();
const controller = require('../controllers/employersCTRL');

/* GET home page. */
router.get('/add', controller.employeeCreatePage);
router.post('/add', controller.employeeCreateEmployee);
router.get('/', controller.employeeList);
router.get('/add/:id', controller.employeeUpdatePage);
router.get('/delete/:id', controller.employeeDelete)
module.exports = router;
